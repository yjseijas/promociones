﻿using Promociones.Models;
using Promociones.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Promociones.Services
{
    public class PromocionService : IPromocionService
    {
        public string AddEdit(Promocion promocion, bool isEditing = false)
        {
            return PromocionRepository.AddEditPromocion(promocion, isEditing);
        }

        public string Delete(Guid id)
        {
            return PromocionRepository.DeletePromocion(id);
        }

        public IEnumerable<Promocion> GetAll()
        {
            return PromocionRepository.GetAll();
        }
        public IEnumerable<Reporte> GetReportes(Guid? id,
                                                  string medioPago,
                                                  string categoria,
                                                  string banco,
                                                  bool? activo,
                                                  DateTime? fechaVigente,
                                                  DateTime? fechaVigenteDesde,
                                                  DateTime? fechaVigenteHasta)
        {
            return PromocionRepository.Reports(id, medioPago, categoria, banco, activo, fechaVigente, fechaVigenteDesde, fechaVigenteHasta);
        }
    }
}