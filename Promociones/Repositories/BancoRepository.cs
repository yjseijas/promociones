﻿using Promociones.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Promociones.Repositories
{
    public class BancoRepository
    {
        static string cadenaConexion = @"data source=localhost\SQLEXPRESS;initial catalog=Promociones;Integrated Security = true";

        public static List<Banco> GetBancos() //yjs 071121
        {
            List<Banco> listaBanco = new List<Banco>();

            string sql = "select distinct * from Banco  ";

            SqlConnection con = new SqlConnection(cadenaConexion);
            con.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(sql, con);
            DataSet dsResultado = new DataSet();
            adapter.Fill(dsResultado, "VistaBanco");
            DataTable tblCliente;
            tblCliente = dsResultado.Tables["VistaBanco"];

            foreach (DataRow drCurrent in tblCliente.Rows)
            {
                Banco bancos = new Banco()
                {
                    Id = (Guid)drCurrent["Id"],
                    NombreBanco = drCurrent["NombreBanco"].ToString(),
                    Activo = (Boolean)drCurrent["Activo"]
                };

                listaBanco.Add(bancos);
            }
            return listaBanco;
        }
    }
}