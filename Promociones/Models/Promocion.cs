﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Promociones.Models
{
    public class Promocion
    {
        public Guid Id { get; private set; }
        public IEnumerable<string> MediosDePago { get; private set; }
        public IEnumerable<string> Bancos { get; private set; }
        public IEnumerable<string> CategoriasProductos { get; private set; }
        public int? MaximaCantidadDeCuotas { get; private set; }
        public decimal? PorcentajeDeDescuento { get; private set; }
        public decimal? ValorInteresCuota { get; private set; }
        public DateTime? FechaInicio { get; private set; }
        public DateTime? FechaFin { get; private set; }
        public bool Activo { get; private set; }
        public DateTime FechaCreacion { get; private set; }
        public DateTime? FechaModificacion { get; private set; }
        public Promocion(
                            Guid id,
                            IEnumerable<string> mediosDePago,
                            IEnumerable<string> bancos,
                            IEnumerable<string> categoriasProductos,
                            int? maximaCantidadDeCuotas,
                            decimal? valorInteresCuota,
                            decimal? porcentajeDeDescuento,
                            DateTime? fechaInicio,
                            DateTime? fechaFin,
                            bool activo,
                            DateTime fechaCreacion,
                            DateTime? fechaModificacion
                        ) 
        { 
            this.Id = id;
            this.MediosDePago = mediosDePago;
            this.Bancos = bancos;
            this.CategoriasProductos = categoriasProductos;
            this.MaximaCantidadDeCuotas = maximaCantidadDeCuotas;
            this.ValorInteresCuota = valorInteresCuota;
            this.PorcentajeDeDescuento = porcentajeDeDescuento;
            this.FechaInicio = fechaInicio;
            this.FechaFin = fechaFin;
            this.Activo = activo;
            this.FechaCreacion = fechaCreacion;
            this.FechaModificacion = fechaModificacion;
        }
    }
}