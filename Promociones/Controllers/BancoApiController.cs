﻿using Promociones.Models;
using Promociones.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Promociones.Controllers
{
    public class BancoApiController : ApiController
    {
        private IBancoService _bancoService;
        // GET: Banco
        public BancoApiController()
        {
            _bancoService = new BancoService();
        }
        //public IEnumerable<Banco> Geta()
        //{
        //    return _bancoService.FindAll();
        //}
        public HttpResponseMessage Get()
        {
            var resp =  _bancoService.FindAll();

            if (resp == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No se encontró el banco");
            }

            return Request.CreateResponse(HttpStatusCode.OK, resp);
        }
    }
}