﻿using Promociones.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promociones.Services
{
    public interface IPromocionService
    {
        IEnumerable<Promocion> GetAll();
        string AddEdit(Promocion promocion, bool isEditing = false);
        string Delete(Guid id);

        IEnumerable<Reporte> GetReportes(Guid? id,
                                                  string medioPago,
                                                  string categoria,
                                                  string banco,
                                                  bool? activo,
                                                  DateTime? fechaVigente, 
                                                  DateTime? fechaVigenteDesde, 
                                                  DateTime? fechaVigenteHasta);
    }
}
