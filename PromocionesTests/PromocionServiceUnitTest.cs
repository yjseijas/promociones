﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promociones.Models;
using Promociones.Services;
using System;

namespace PromocionesTests
{
    [TestClass]
    public class PromocionServiceUnitTest
    {
        [TestMethod]
        public void GetAllTest()
        {
            //Arrange
            var promocionService = new PromocionService();

            //Act
            var result = promocionService.GetAll();
            //Assert

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddEditTest()
        {
            //Arrange
            var promocionService = new PromocionService();
            string[] array01 = new string[0];
            var promocion = new Promocion(new Guid(), array01, array01, array01, null, null, null, null, null, false, DateTime.Today, null);
            var promocion02 = new Promocion(Guid.NewGuid(), array01, array01, array01, 10, 5, null, null, null, false, DateTime.Today, null);

            //Act
            var result = promocionService.AddEdit(promocion, false);
            var result02 = promocionService.AddEdit(promocion, true);
            var result03 = promocionService.AddEdit(promocion02, true);
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("CANTIDAD_CUOTAS_PORCENTAJE_DESCUENTO_AL_MENOS_UNO_NECESITA_VALOR", result);
            Assert.AreEqual("ID_PROMOCIÓN_NO_PUEDE_SER_NULO_O_VACIO", result02);
            Assert.AreEqual("NO_EXISTE_PROMOCION_A_MODIFICAR", result03);
        }
        [TestMethod]
        public void ReportsTest()
        {
            //Arrange
            var promocionService = new PromocionService();
            var currentDate = DateTime.Today;

            //Act
            var getCurrentPromotions = promocionService.GetReportes(null, null, null, null, true, currentDate, null, null);
            //Assert
            Assert.IsNotNull(getCurrentPromotions);
        }
    }
}
