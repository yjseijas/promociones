USE [master]
GO
/****** Object:  Database [Promociones]    Script Date: 24/11/2021 14:07:00 ******/
CREATE DATABASE [Promociones]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Promociones', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Promociones.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Promociones_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Promociones_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Promociones] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Promociones].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Promociones] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Promociones] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Promociones] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Promociones] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Promociones] SET ARITHABORT OFF 
GO
ALTER DATABASE [Promociones] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Promociones] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Promociones] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Promociones] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Promociones] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Promociones] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Promociones] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Promociones] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Promociones] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Promociones] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Promociones] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Promociones] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Promociones] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Promociones] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Promociones] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Promociones] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Promociones] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Promociones] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Promociones] SET  MULTI_USER 
GO
ALTER DATABASE [Promociones] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Promociones] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Promociones] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Promociones] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Promociones] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Promociones] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [Promociones] SET QUERY_STORE = OFF
GO
USE [Promociones]
GO
/****** Object:  UserDefinedFunction [dbo].[ExisteItem]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ExisteItem](@items varchar(max), @item varchar(max))
returns int
as
begin
	declare @resp bit;

	if  @item in (select value from string_split(@items, '/'))
	begin
		return 1
	end;

	return 0
end;
GO
/****** Object:  UserDefinedFunction [dbo].[ExistenBancos]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ExistenBancos] (@bancos varchar(max))
returns int
as
begin
	declare @banco varchar(400)

	DECLARE bancos_cursor CURSOR FOR   
		select value from string_split(@bancos, '/')

	open bancos_cursor

	fetch next from bancos_cursor
		into @banco

	while @@FETCH_STATUS = 0
	begin
		begin
			if  @banco not in (	select NombreBanco from Banco (nolock) where Activo = 1 )
			begin
				CLOSE bancos_cursor;  
				DEALLOCATE bancos_cursor; 
				return 0
			end;
		end;
		FETCH NEXT FROM bancos_cursor   
			INTO @banco  
	end;
	CLOSE bancos_cursor;  
	DEALLOCATE bancos_cursor;
	return 1
end;






GO
/****** Object:  UserDefinedFunction [dbo].[ExistenCategorias]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ExistenCategorias] (@categorias varchar(max))
returns int
as
begin
	declare @categoria varchar(400)

	DECLARE categorias_cursor CURSOR FOR   
		select value from string_split(@categorias, '/')

	open categorias_cursor

	fetch next from categorias_cursor
		into @categoria

	while @@FETCH_STATUS = 0
	begin
		begin
			if  @categoria not in (	select NombreCategoriaProducto from CategoriaProducto (nolock) where Activo = 1 )
			begin
				CLOSE categorias_cursor;  
				DEALLOCATE categorias_cursor; 
				return 0
			end;
		end;
		FETCH NEXT FROM categorias_cursor   
			INTO @categoria  
	end;
	CLOSE categorias_cursor;  
	DEALLOCATE categorias_cursor;
	return 1
end;

--declare @resp  int

--select @resp = dbo.ExistenMediosPago('TARJETA_CREDITO/TARJETA_DEBITO')

--select @resp



GO
/****** Object:  UserDefinedFunction [dbo].[ExistenMediosPago]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ExistenMediosPago] (@medios varchar(max))
returns int
as
begin
	declare @medio varchar(400)

	DECLARE medios_cursor CURSOR FOR   
		select value from string_split(@medios, '/')

	open medios_cursor

	fetch next from medios_cursor
		into @medio

	while @@FETCH_STATUS = 0
	begin
		begin
			if  @medio not in (	select NombreMedioPago from MedioDePago (nolock) where Activo = 1 )
			begin
				CLOSE medios_cursor;  
				DEALLOCATE medios_cursor; 
				return 0
			end;
		end;
		FETCH NEXT FROM medios_cursor   
			INTO @medio  
	end;
	CLOSE medios_cursor;  
	DEALLOCATE medios_cursor;
	return 1
end;

--declare @resp  int

--select @resp = dbo.ExistenBancos('Galicia/Macros')

--select @resp



GO
/****** Object:  UserDefinedFunction [dbo].[SolapaBancos]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[SolapaBancos] (@lista varchar(400),
									  @fechaInicio datetime,
									  @fechaFin datetime,
									  @id uniqueidentifier = null,
									  @isEditing bit = null)
returns int
as
begin
	declare @banco varchar(400)
	declare @banco_split varchar(400)

	if @isEditing = 0
	begin
		DECLARE bancos_curso CURSOR FOR   
		select Bancos 
		from Promocion (nolock)
		where ((@fechaInicio between FechaInicio and FechaFin) or 
				(@fechaFin between FechaInicio and FechaFin))
				and Activo = 1
	end;
	
	if @isEditing = 1
	begin
		DECLARE bancos_curso CURSOR FOR   
		select Bancos 
		from Promocion (nolock)
		where ((@fechaInicio between FechaInicio and FechaFin) or 
				(@fechaFin between FechaInicio and FechaFin))
				and Activo = 1
				and Id <> @id
	end;

	open bancos_curso

	fetch next from bancos_curso
	into @banco

	while @@FETCH_STATUS = 0
	begin
		declare bancos_split_cursor CURSOR FOR
			select value from string_split(@banco, '/')

		open bancos_split_cursor

		fetch next from bancos_split_cursor
		into @banco_split

		while @@FETCH_STATUS = 0
		begin
			if  @banco_split in (select value from string_split(@lista, '/'))
			begin
				CLOSE bancos_split_cursor;  
				DEALLOCATE bancos_split_cursor; 
				CLOSE bancos_curso;  
				DEALLOCATE bancos_curso; 
				return 1
			end;
			fetch next from bancos_split_cursor
			into @banco_split
		end;
		CLOSE bancos_split_cursor;  
		DEALLOCATE bancos_split_cursor; 

		FETCH NEXT FROM bancos_curso   
		INTO @banco  
	end;
	CLOSE bancos_curso;  
	DEALLOCATE bancos_curso;
	return 0
end;






GO
/****** Object:  UserDefinedFunction [dbo].[SolapaCategorias]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[SolapaCategorias] (@lista varchar(400),
									  @fechaInicio datetime,
									  @fechaFin datetime,
									  @id uniqueidentifier = null,
									  @isEditing bit = null)
returns int
as
begin
	declare @categoria varchar(400)
	declare @categoria_split varchar(400)

	if @isEditing = 0
	begin
		DECLARE categorias_curso CURSOR FOR   
			select CategoriasProductos 
			from Promocion (nolock)
			where ((@fechaInicio between FechaInicio and FechaFin) or 
				   (@fechaFin between FechaInicio and FechaFin))
				   and Activo = 1
	end;

	if @isEditing = 1
	begin
		DECLARE categorias_curso CURSOR FOR   
			select CategoriasProductos 
			from Promocion (nolock)
			where ((@fechaInicio between FechaInicio and FechaFin) or 
				   (@fechaFin between FechaInicio and FechaFin))
				   and Activo = 1
				   and Id <> @id
	end;

	open categorias_curso

	fetch next from categorias_curso
	into @categoria

	while @@FETCH_STATUS = 0
	begin
		declare categorias_split_cursor CURSOR FOR
			select value from string_split(@categoria, '/')

		open categorias_split_cursor

		fetch next from categorias_split_cursor
		into @categoria_split

		while @@FETCH_STATUS = 0
		begin
			if  @categoria_split in (select value from string_split(@lista, '/'))
			begin
				CLOSE categorias_split_cursor;  
				DEALLOCATE categorias_split_cursor; 
				CLOSE categorias_curso;  
				DEALLOCATE categorias_curso; 
				return 1
			end;
			fetch next from categorias_split_cursor
			into @categoria_split
		end;
		CLOSE categorias_split_cursor;  
		DEALLOCATE categorias_split_cursor; 

		FETCH NEXT FROM categorias_curso   
		INTO @categoria  
	end;
	CLOSE categorias_curso;  
	DEALLOCATE categorias_curso;
	return 0
end;






GO
/****** Object:  UserDefinedFunction [dbo].[SolapaMediosPago]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[SolapaMediosPago] (@lista varchar(400),
									  @fechaInicio datetime,
									  @fechaFin datetime,
									  @id uniqueidentifier = null,
									  @isEditing bit = null)
returns int
as
begin
	declare @medio varchar(400)
	declare @medio_split varchar(400)

	if @isEditing = 0
	begin
		DECLARE medios_cursor CURSOR FOR   
			select MediosDePago 
			from Promocion (nolock)
			where ((@fechaInicio between FechaInicio and FechaFin) or 
				   (@fechaFin between FechaInicio and FechaFin))
				   and Activo = 1
	end;

	if @isEditing = 1
	begin
		DECLARE medios_cursor CURSOR FOR   
			select MediosDePago 
			from Promocion (nolock)
			where ((@fechaInicio between FechaInicio and FechaFin) or 
				   (@fechaFin between FechaInicio and FechaFin))
				   and Activo = 1
				   and Id <> @id
	end;
	open medios_cursor

	fetch next from medios_cursor
	into @medio

	while @@FETCH_STATUS = 0
	begin
		declare medios_split_cursor CURSOR FOR
			select value from string_split(@medio, '/')

		open medios_split_cursor

		fetch next from medios_split_cursor
		into @medio_split

		while @@FETCH_STATUS = 0
		begin
			if  @medio_split in (select value from string_split(@lista, '/'))
			begin
				CLOSE medios_split_cursor;  
				DEALLOCATE medios_split_cursor; 
				CLOSE medios_cursor;  
				DEALLOCATE medios_cursor; 
				return 1
			end;
			fetch next from medios_split_cursor
			into @medio_split
		end;
		CLOSE medios_split_cursor;  
		DEALLOCATE medios_split_cursor; 

		FETCH NEXT FROM medios_cursor   
		INTO @medio  
	end;
	CLOSE medios_cursor;  
	DEALLOCATE medios_cursor;
	return 0
end;






GO
/****** Object:  Table [dbo].[Banco]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Banco](
	[Id] [uniqueidentifier] NOT NULL,
	[NombreBanco] [nvarchar](50) NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_Banco] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoriaProducto]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoriaProducto](
	[Id] [uniqueidentifier] NOT NULL,
	[NombreCategoriaProducto] [varchar](50) NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_CategoriaProducto] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MedioDePago]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedioDePago](
	[Id] [uniqueidentifier] NOT NULL,
	[NombreMedioPago] [nvarchar](50) NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_MedioDePago] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Promocion]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Promocion](
	[Id] [uniqueidentifier] NOT NULL,
	[MediosDePago] [nvarchar](max) NULL,
	[Bancos] [nvarchar](max) NULL,
	[CategoriasProductos] [nvarchar](max) NULL,
	[MaximaCantidadDeCuotas] [int] NULL,
	[ValorInteresesCuotas] [decimal](18, 0) NULL,
	[PorcentajeDeDescuento] [decimal](18, 0) NULL,
	[FechaInicio] [datetime] NULL,
	[FechaFin] [datetime] NULL,
	[Activo] [bit] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NULL,
 CONSTRAINT [PK_Promocion_] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spAddEditPromocion]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spAddEditPromocion] (@id uniqueidentifier,
									 @mediosDePago varchar(MAX),
									 @bancos varchar(MAX),
									 @categoriasProductos varchar(MAX),
									 @maximaCantidadDeCuotas int = null,
									 @valorInteresCuotas Decimal = null,
									 @porcentajeDeDescuento Decimal = null,
									 @fechaInicio Datetime = null,
									 @fechaFin Datetime = null,
									 @activo bit,
									 @fechaCreacion Datetime,
									 @fechaMoficacion Datetime = null,
									 @isEditing bit,
									 @RequestStatus int OUT) as            

if not exists (select FechaCreacion from Promocion where Id = @id) and @isEditing = 1
begin
  set @RequestStatus = 50
  return @RequestStatus   
end;

if exists (select FechaCreacion from Promocion where Id = @id) and @isEditing = 0
begin
  set @RequestStatus = 56
  return @RequestStatus   
end;
declare @resp int

select @resp = dbo.SolapaMediosPago(@mediosDePago, @fechaInicio, @fechaFin, @id, @isEditing)

if @resp = 1
begin
  set @RequestStatus = 53	
  return @RequestStatus  
end;

select @resp = dbo.SolapaBancos(@bancos, @fechaInicio, @fechaFin, @id, @isEditing)

if @resp = 1
begin
  set @RequestStatus = 54	
  return @RequestStatus  
end;

select @resp = dbo.SolapaCategorias(@categoriasProductos, @fechaInicio, @fechaFin, @id, @isEditing)

if @resp = 1
begin
  set @RequestStatus = 55	
  return @RequestStatus  
end;

select @resp = dbo.ExistenMediosPago(@mediosDePago)

if @resp = 0
begin
  set @RequestStatus = 63	
  return @RequestStatus  
end;

select @resp = dbo.ExistenBancos(@bancos)

if @resp = 0
begin
  set @RequestStatus = 64	
  return @RequestStatus  
end;

select @resp = dbo.ExistenCategorias(@categoriasProductos)

if @resp = 0
begin
  set @RequestStatus = 65	
  return @RequestStatus  
end;


if exists (select FechaCreacion from Promocion where Id = @id)     
begin        
 update Promocion set 
	MediosDePago = @mediosDePago,
	Bancos = @bancos,
	CategoriasProductos = @categoriasProductos,
	MaximaCantidadDeCuotas = @maximaCantidadDeCuotas,
	ValorInteresesCuotas = @valorInteresCuotas,
	PorcentajeDeDescuento = @porcentajeDeDescuento,
	FechaInicio = @fechaInicio,
	FechaFin = @fechaFin,
	Activo = @activo,
	FechaModificacion = @fechaMoficacion 	  
  where Id = @id  
  set @RequestStatus = 2
 return @RequestStatus      
end;         
insert into Promocion (Id,
					   MediosDePago,
					   Bancos,
					   CategoriasProductos,
					   MaximaCantidadDeCuotas,
					   ValorInteresesCuotas,
					   PorcentajeDeDescuento,
					   FechaInicio,
					   FechaFin,
					   Activo,
					   FechaCreacion,
					   FechaModificacion
					   ) values 
					   (@id,
					    @mediosDePago,
						@bancos,
						@categoriasProductos,
						@maximaCantidadDeCuotas,
						@valorInteresCuotas,
						@porcentajeDeDescuento,
						@fechaInicio,
						@fechaFin,
						@activo,
						@fechaCreacion,
						@fechaMoficacion
					   )    
set @RequestStatus = 1
return @RequestStatus
GO
/****** Object:  StoredProcedure [dbo].[spDeletePromocion]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spDeletePromocion] (@id uniqueidentifier,
									 @RequestStatus int OUT) as            

if not exists (select FechaCreacion from Promocion where Id = @id)
begin
  set @RequestStatus = 66
  return @RequestStatus   
end;

 update Promocion set 
	Activo = 0
	where Id = @id
	
set @RequestStatus = 3
return @RequestStatus
GO
/****** Object:  StoredProcedure [dbo].[spReports]    Script Date: 24/11/2021 14:07:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spReports] ( @id uniqueidentifier = null,
							@medioDePago varchar(MAX) = null,
							@categoria varchar(MAX) = null,
							@banco varchar(MAX) = null,
							@vigentes bit = null,
							@fechaVigente Datetime = null,
							@fechaVigenteDesde Datetime = null,
							@fechaVigenteHasta Datetime = null) as    
							
declare @paramMedio varchar(max) = '%' + @medioDePago + '%'
declare @paramCategoria varchar(max) = '%' + @categoria + '%'
declare @paramBanco varchar(max) = '%' + @banco + '%'

select Id, 
	   MediosDePago,
	   CategoriasProductos, 
	   Bancos, 
	   isnull(MaximaCantidadDeCuotas, 0) MaximaCantidadDeCuotas,
	   isnull(ValorInteresesCuotas, 0) ValorInteresesCuotas,
	   isnull(PorcentajeDeDescuento, 0) PorcentajeDeDescuento
	from Promocion (nolock)
	where 
		(Id = @id or @id is null) and
		((dbo.ExisteItem(Promocion.MediosDePago,@medioDePago) = 1 or @medioDePago is null ))  and
		((dbo.ExisteItem(Promocion.CategoriasProductos,@categoria) = 1) or @categoria is null) and
		((dbo.ExisteItem(Promocion.Bancos,@banco) = 1) or @banco is null) and
		(Activo = 1 or @vigentes is null) and 
		((@fechaVigente between FechaInicio and FechaFin) or @fechaVigente is null)

GO
USE [master]
GO
ALTER DATABASE [Promociones] SET  READ_WRITE 
GO
