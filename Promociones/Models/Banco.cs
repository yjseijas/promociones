﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Promociones.Models
{
    public class Banco
    {
        public Guid Id { get; set; }
        public string NombreBanco { get; set; }
        public bool Activo { get; set; }
    }
}