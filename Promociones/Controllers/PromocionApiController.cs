﻿using Promociones.Models;
using Promociones.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Promociones.Controllers
{
    public class PromocionApiController : ApiController
    {
        private IPromocionService _promocionService;

        public PromocionApiController()
        {
            _promocionService = new PromocionService();
        }
        // GET: PromoionesApi
        public HttpResponseMessage GetAll()
        {
            var resp = _promocionService.GetAll();
            if (resp == null || resp.Count() == 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No hay promociones cargadas.");
            }

            return Request.CreateResponse(HttpStatusCode.OK, resp);
        }

        public HttpResponseMessage GetOnePromotion(Guid id)
        {
            var resp = _promocionService.GetReportes(id, null, null, null, null, null, null, null);

            if (resp == null || resp.Count() == 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No existe promoción con ese ID.");
            }

            return Request.CreateResponse(HttpStatusCode.OK, resp);
        }
        public HttpResponseMessage GetCurrentPromotions(bool activo)
        {
            var currentDate = DateTime.Today;
            var resp = _promocionService.GetReportes(null, null, null, null, activo, currentDate, null, null);

            if (resp == null || resp.Count() == 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No hay promociones activas para la fecha actual.");
            }

            return Request.CreateResponse(HttpStatusCode.OK, resp);
        }

        public HttpResponseMessage GetPromotionsByDate(DateTime dateCurrent)
        {
            var resp = _promocionService.GetReportes(null, null, null, null, true, dateCurrent, null, null);

            if (resp == null || resp.Count() == 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No hay promociones activas para la fecha indicada.");
            }

            return Request.CreateResponse(HttpStatusCode.OK, resp);
        }
        public HttpResponseMessage GetCurrentPromotionsBySale(string medioPago, string banco, string categoria)
        {
            var currentDate = DateTime.Today;
            var resp = _promocionService.GetReportes(null, medioPago, categoria, banco, true, currentDate, null, null);

            if (resp == null || resp.Count() == 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No hay promociones activas para la fecha actual.");
            }

            return Request.CreateResponse(HttpStatusCode.OK, resp);
        }
            public HttpResponseMessage Post([FromBody] Promocion promocion)
        {
            var resp =_promocionService.AddEdit(promocion);

            if (resp.Contains("OCURRIO_UN_ERROR_INESPERADO"))
                return Request.CreateResponse(HttpStatusCode.InternalServerError, resp);

            if (resp == "ID_PROMOCIÓN_NO_PUEDE_SER_NULO_O_VACIO" ||
                resp.Contains("PROMOCION_SE_SOLAPA_CON") ||
                resp.Contains("NECESITA_VALOR") ||
                resp.Contains("YA_EXISTE") ||
                resp.Contains("PORCENTAJE") || 
                resp.Contains("FECHA") ||
                resp.Contains("ADMITEN") ||
                resp.Contains("NO_ES_VALID"))
                return Request.CreateResponse(HttpStatusCode.BadRequest, resp);

            return Request.CreateResponse(HttpStatusCode.OK, resp);
        }

        public HttpResponseMessage Put([FromBody] Promocion promocion)
        {
            var resp = _promocionService.AddEdit(promocion, true);

            if (resp == "NO_EXISTE_PROMOCION_A_MODIFICAR")
                return Request.CreateResponse(HttpStatusCode.NotFound, resp);

            if (resp == "ID_PROMOCIÓN_NO_PUEDE_SER_NULO_O_VACIO" ||
                resp.Contains("PROMOCION_SE_SOLAPA_CON") ||
                resp.Contains("NECESITA_VALOR") ||
                resp.Contains("YA_EXISTE") ||
                resp.Contains("PORCENTAJE") ||
                resp.Contains("FECHA") ||
                resp.Contains("ADMITEN") ||
                resp.Contains("NO_ES_VALID"))
                return Request.CreateResponse(HttpStatusCode.BadRequest, resp);

            if (resp.Contains("OCURRIO_UN_ERROR_INESPERADO"))
                return Request.CreateResponse(HttpStatusCode.InternalServerError, resp);


            return Request.CreateResponse(HttpStatusCode.OK, resp);
        }

        public HttpResponseMessage Delete(Guid id)
        {
            var resp = _promocionService.Delete(id);

            if (resp == "NO_EXISTE_PROMOCION_A_ELIMINAR")
                return Request.CreateResponse(HttpStatusCode.NotFound, resp);

            if (resp.Contains("OCURRIO_UN_ERROR_INESPERADO"))
                return Request.CreateResponse(HttpStatusCode.InternalServerError, resp);


            return Request.CreateResponse(HttpStatusCode.OK, resp);
        }
        //// GET: PromoionesApi/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: PromoionesApi/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: PromoionesApi/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: PromoionesApi/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: PromoionesApi/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: PromoionesApi/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: PromoionesApi/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
