﻿using Promociones.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Promociones.Repositories
{
    public class PromocionRepository
    {
        static string cadenaConexion = @"data source=localhost\SQLEXPRESS;initial catalog=Promociones;Integrated Security = true";

        private enum Results
        {
            AGREGADO_CORRECTAMENTE = 1,
            ACTUALIZADO_CORRECTAMENTE = 2,
            ELIMINADO_CORRECTAMENTE = 3,
            NO_EXISTE_PROMOCION_A_MODIFICAR = 50,
            ID_PROMOCIÓN_NO_PUEDE_SER_NULO_O_VACIO = 51,
            OCURRIO_UN_ERROR_INESPERADO = 52,
            PROMOCION_SE_SOLAPA_CON_MEDIO_DE_PAGO = 53,
            PROMOCION_SE_SOLAPA_CON_BANCO = 54,
            PROMOCION_SE_SOLAPA_CON_CATEGORIA = 55,
            YA_EXISTE_PROMOCION_CON_ESE_ID = 56,
            CANTIDAD_CUOTAS_PORCENTAJE_DESCUENTO_AL_MENOS_UNO_NECESITA_VALOR = 57,
            PORCENTAJE_DESCUENTO_DEBE_ESTAR_ENTRE_5_Y_80 = 58,
            PORCENTAJE_INTERES_NO_PUEDE_TENER_VALOR_SIN_CANTIDAD_DE_CUOTAS = 59,
            FECHA_INICIO_NO_PUEDE_SER_MAYOR_A_FECHA_FIN = 60,
            NO_SE_ADMITEN_VALORES_NEGATIVOS = 61,
            CANTIDAD_CUOTAS_PORCENTAJE_DESCUENTO_AMBAS_NO_PUEDEN_TENER_VALOR = 62,
            MEDIO_DE_PAGO_SUMINISTRADO_NO_ES_VALIDO = 63,
            BANCO_SUMINISTRADO_NO_ES_VALIDO = 64,
            CATEGORIA_SUMINISTRADO_NO_ES_VALIDA = 65,
            NO_EXISTE_PROMOCION_A_ELIMINAR = 66
        }

        public static List<Promocion> GetAll() //yjs 141121
        {
            List<Promocion> listaPromociones = new List<Promocion>();

            string sql = "select distinct * from Promocion (nolock)  ";

            SqlConnection con = new SqlConnection(cadenaConexion);
            con.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(sql, con);
            DataSet dsResultado = new DataSet();
            adapter.Fill(dsResultado, "VistaPromociones");
            DataTable tblPromocion;
            tblPromocion = dsResultado.Tables["VistaPromociones"];
            var zeroDecimal = (decimal)0.0;
            DateTime? fechaInicio = null;
            DateTime? fechaFin = null;
            DateTime? fechaModificacion = null;

            foreach (DataRow drCurrent in tblPromocion.Rows)
            {
                var id = (Guid)drCurrent["Id"];
                var listMediosPago = drCurrent["MediosDePago"].ToString().Split('/');
                var listBancos = drCurrent["Bancos"].ToString().Split('/');
                var listCategoriasProductos = drCurrent["CategoriasProductos"].ToString().Split('/');
                var maximaCantidadDeCuotas = String.IsNullOrEmpty(drCurrent["MaximaCantidadDeCuotas"].ToString()) 
                                             ? 0
                                             : Int32.Parse(drCurrent["MaximaCantidadDeCuotas"].ToString());

                var valorInteresCuotas = String.IsNullOrEmpty(drCurrent["ValorInteresesCuotas"].ToString())
                                          ? zeroDecimal
                                          : Convert.ToDecimal(drCurrent["ValorInteresesCuotas"].ToString());

                var porcentajeDeDescuento = String.IsNullOrEmpty(drCurrent["PorcentajeDeDescuento"].ToString())
                                          ? zeroDecimal
                                          : Convert.ToDecimal(drCurrent["PorcentajeDeDescuento"].ToString());

                if(!String.IsNullOrEmpty(drCurrent["FechaInicio"].ToString()))
                {
                    fechaInicio = DateTime.Parse(drCurrent["FechaInicio"].ToString());
                }

                if (!String.IsNullOrEmpty(drCurrent["FechaFin"].ToString()))
                {
                    fechaFin = DateTime.Parse(drCurrent["FechaFin"].ToString());
                }

                var activo = bool.Parse(drCurrent["Activo"].ToString());

                var fechaCreacion = DateTime.Parse(drCurrent["FechaCreacion"].ToString());

                if (!String.IsNullOrEmpty(drCurrent["FechaModificacion"].ToString()))
                {
                    fechaModificacion = DateTime.Parse(drCurrent["FechaModificacion"].ToString());
                }

                var promociones = new Promocion(
                                                    id,
                                                    listMediosPago,
                                                    listBancos,
                                                    listCategoriasProductos,
                                                    maximaCantidadDeCuotas,
                                                    valorInteresCuotas,
                                                    porcentajeDeDescuento,
                                                    fechaInicio,
                                                    fechaFin,
                                                    activo,
                                                    fechaCreacion,
                                                    fechaModificacion
                                                ) { };
               
                listaPromociones.Add(promociones);
            }
            return listaPromociones;
        }

        public static List<Reporte> Reports(Guid? id, 
                                              string medioPago = null,
                                              string categoria = null,
                                              string banco = null, 
                                              bool? activo = null,
                                              DateTime? fechaVigente = null,
                                              DateTime? fechaVigenteDesde = null,
                                              DateTime? fechaVigenteHasta = null) //yjs 201121
        {
            var listaReporte = new List<Reporte>();

            SqlConnection con = new SqlConnection(cadenaConexion);
            var  dataTable = new DataTable();

            using (SqlCommand cmd = new SqlCommand("spReports", con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@medioDePago", medioPago);
                cmd.Parameters.AddWithValue("@banco", banco);
                cmd.Parameters.AddWithValue("@categoria", categoria);
                cmd.Parameters.AddWithValue("@vigentes", activo);
                cmd.Parameters.AddWithValue("@fechaVigente", fechaVigente);
                cmd.Parameters.AddWithValue("@fechaVigenteDesde", fechaVigenteDesde);
                cmd.Parameters.AddWithValue("@fechaVigenteHasta", fechaVigenteHasta);

                var adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dataTable);

                foreach (DataRow drCurrent in dataTable.Rows)
                {
                    Reporte reporte = new Reporte();

                    reporte.Id = (Guid)drCurrent["Id"];
                    reporte.MedioDePago = drCurrent["MediosDePago"].ToString();
                    reporte.Banco = drCurrent["Bancos"].ToString(); ;
                    reporte.CategoriaProducto = drCurrent["CategoriasProductos"].ToString(); ;
                    reporte.MaximaCantidadDeCuotas = Int32.Parse(drCurrent["MaximaCantidadDeCuotas"].ToString());
                    reporte.ValorInteresCuota = Decimal.Parse(drCurrent["ValorInteresesCuotas"].ToString());
                    reporte.PorcentajeDeDescuento = Decimal.Parse(drCurrent["PorcentajeDeDescuento"].ToString());

                    listaReporte.Add(reporte);
                }
            }

            return listaReporte;
        }
        public static string AddEditPromocion(Promocion promocion, bool isEditing = false ) //yjs 141121
        {
            try
            {
                var validations = Validations(promocion, isEditing);
                if (validations != "")               
                    return validations;

                int reg = 0;
                var idPromocion = (promocion.Id == null || promocion.Id == Guid.Empty) ? Guid.NewGuid() : promocion.Id;
                var mediosPago = string.Join("/", promocion.MediosDePago);
                mediosPago = string.IsNullOrEmpty(mediosPago) ? "null" : mediosPago;

                var bancos = string.Join("/", promocion.Bancos);
                bancos = string.IsNullOrEmpty(bancos) ? "null" : bancos;

                var categoriasProductos = string.Join("/", promocion.CategoriasProductos);
                categoriasProductos = string.IsNullOrEmpty(categoriasProductos) ? "null" : categoriasProductos;

                int result;
                bool activo;
                if (isEditing)
                {
                    activo = promocion.Activo;
                }
                else
                {
                    activo = true;
                }
             
                var fechaCreacion = DateTime.Now;

                SqlConnection con = new SqlConnection(cadenaConexion);

                using (SqlCommand cmd = new SqlCommand("spAddEditPromocion", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", idPromocion);
                    cmd.Parameters.AddWithValue("@mediosDePago", mediosPago);
                    cmd.Parameters.AddWithValue("@bancos", bancos);
                    cmd.Parameters.AddWithValue("@categoriasProductos", categoriasProductos);
                    cmd.Parameters.AddWithValue("@maximaCantidadDeCuotas", promocion.MaximaCantidadDeCuotas);
                    cmd.Parameters.AddWithValue("@valorInteresCuotas", promocion.ValorInteresCuota);
                    cmd.Parameters.AddWithValue("@porcentajeDeDescuento", promocion.PorcentajeDeDescuento);
                    cmd.Parameters.AddWithValue("@fechaInicio", promocion.FechaInicio);
                    cmd.Parameters.AddWithValue("@fechaFin", promocion.FechaFin);
                    cmd.Parameters.AddWithValue("@activo", activo);
                    cmd.Parameters.AddWithValue("@fechaCreacion", fechaCreacion);
                    cmd.Parameters.AddWithValue("@isEditing", isEditing);
                    if (isEditing)
                    {
                        cmd.Parameters.AddWithValue("@fechaMoficacion", DateTime.Now);
                    }

                    var returnValue = new SqlParameter("@RequestStatus", SqlDbType.Int);
                    returnValue.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnValue);
                 
                    reg = cmd.ExecuteNonQuery();
                    var requestStatus = (int)cmd.Parameters["@RequestStatus"].Value;
                    result = requestStatus;
                    con.Close();
                }

                var retorno = (Results)result;

                var textInicial = result < 50 ? "Registro " : "";
                var idResp = result < 50 ? ". ID -> " + idPromocion : "";
                return textInicial + retorno + idResp;

            }
            catch (Exception ex)
            {
                var mensaje = "" + (Results)52;
                return mensaje + ": "+ ex;
            }
        }

        public static string DeletePromocion(Guid id) //yjs 191121
        {
            try
            {
                int reg = 0;
                int result;

                SqlConnection con = new SqlConnection(cadenaConexion);

                using (SqlCommand cmd = new SqlCommand("spDeletePromocion", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", id);

                    var returnValue = new SqlParameter("@RequestStatus", SqlDbType.Int);
                    returnValue.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnValue);

                    reg = cmd.ExecuteNonQuery();
                    var requestStatus = (int)cmd.Parameters["@RequestStatus"].Value;
                    result = requestStatus;
                    con.Close();
                }

                var retorno = (Results)result;

                var textInicial = result < 50 ? "Registro " : "";
                var idResp = result < 50 ? ". ID -> " + id : "";
                return textInicial + retorno + idResp;

            }
            catch (Exception ex)
            {
                var mensaje = "" + (Results)52;
                return mensaje + ": " + ex;
            }
        }
        private static string Validations(Promocion promocion, bool isEditing = false)
        {
            var porcentajeDescuento = IsNullDec(promocion.PorcentajeDeDescuento);
            var maximaCantCuotas = IsNullInt(promocion.MaximaCantidadDeCuotas);
            var valorIntCuotas = promocion.ValorInteresCuota;

            if (porcentajeDescuento < 0 || maximaCantCuotas < 0 || valorIntCuotas < 0)
                return "" + (Results)61;

            if ((promocion.Id == null || promocion.Id == Guid.Empty) && isEditing)            
                return "" + (Results)51;

            if (IsNullInt(maximaCantCuotas) == 0 && IsNullDec(porcentajeDescuento) == 0)
                return "" + (Results)57;

            if ((porcentajeDescuento > 0) && (porcentajeDescuento < 5 || porcentajeDescuento > 80))
                return "" + (Results)58;

            if (IsNullInt(maximaCantCuotas) == 0 && IsNullDec(valorIntCuotas) > 0)
                return "" + (Results)59;

            if (promocion.FechaInicio > promocion.FechaFin)
                return "" + (Results)60;

            if (IsNullInt(maximaCantCuotas) > 0 && IsNullDec(porcentajeDescuento) > 0)
                return "" + (Results)62;

            return "";
        }

        private static int? IsNullInt(int? value)
        {
            return value == null ? 0 : value;
        }
        private static decimal? IsNullDec(decimal? value)
        {
            return value == null ? 0 : value;
        }
    }
}