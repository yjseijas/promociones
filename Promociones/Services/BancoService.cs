﻿using Promociones.Models;
using Promociones.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Promociones.Services
{
    public class BancoService : IBancoService
    {
        public bool addEdit(Banco banco)
        {
            throw new NotImplementedException();
        }

        public bool delete(int idBanco)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Banco> FindAll()
        {
            return BancoRepository.GetBancos();
        }

        public IEnumerable<Banco> FindOne(int idBanco)
        {
            throw new NotImplementedException();
        }
    }
}