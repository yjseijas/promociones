﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Promociones.Models
{
    public class Reporte
    {
        public Guid Id { get; set; }
        public string MedioDePago { get; set; }
        public string Banco { get; set; }
        public string CategoriaProducto { get; set; }
        public int? MaximaCantidadDeCuotas { get; set; }
        public decimal? PorcentajeDeDescuento { get; set; }
        public decimal? ValorInteresCuota { get; set; }
    }
}