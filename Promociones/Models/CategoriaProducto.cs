﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Promociones.Models
{
    public class CategoriaProducto
    {
        public Guid Id { get; set; }
        public string NombreCategoriaProducto { get; set; }
        public bool Activo { get; set; }
    }
}