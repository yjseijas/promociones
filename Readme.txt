Saludos!

La Web Api la desarrollé en .Net Standard C#. 
La BDD está en Sql Server. El archivo script_all.sql crea la base de datos y todos sus objetos.
Colección de Postman: Promociones.postman_collection

En cuanto a lo planteado en el chalenge (Cuesta un poco entender algunas cosas, típico de las Users Stories)

Respecto al "solapamiento" al guardar o editar, entendí por "solapar" que no permitiera que se repitiera una promoción
con algun medio de pago/banco/categoria (cualquiera de los tres) en lo que respecta a las fechas inicio/fin. Por ejempaplo:

	Si hay una promoción en la BDD para "Colchones" cuya vigencia va del 01-12-2021 al 15-12-2021, no puedo crear otra 
	de "colchones" del 15-12-21 al 30-12-21. Tendría que ser la fecha inicial superior al 15-12-21.
	
Respecto al punto "Ver listado de promociones vigentes para una venta", no podía ser uno de los parámetros de entrada "Categoría producto: IEnumerable<string>"
y uno de los de salida "Categoría producto: string". Allí tomé los tres parámetros de entrada "string".

Las fechas en Postman van en formato MM-dd-yyyy. Honestamente, no me fue posible hacer que Postmanme hiciera caso, para enviarlas dd-MM-yyyy. Allí se me fue un bien tiempo.

A los unit tests se podría incluirles muchos mas casos de prueba, pero obvio: requiere mas tiempo.
Igualmente hacerles mejoras, como por ejemplo, implementar Mocks para no pegarle a la BDD.


Atent.

Jesús Seijas C.

Software Developer.
