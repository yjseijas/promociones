﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Promociones.Models
{
    public class MedioDePago
    {
        public Guid Id { get; set; }
        public string NombreMedioDePago { get; set; }
        public bool Activo { get; set; }
    }
}