﻿using Promociones.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promociones.Services
{
    public interface IBancoService
    {
        IEnumerable<Banco> FindAll();
        IEnumerable<Banco> FindOne(int idBanco);
        bool addEdit(Banco banco);
        bool delete(int idBanco);
    }
}
